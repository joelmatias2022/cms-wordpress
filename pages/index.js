import Head from "next/head";
import Container from "../components/container";
import MoreStories from "../components/more-stories";
import HeroPost from "../components/hero-post";
import Intro from "../components/intro";
import Layout from "../components/layout";
import { getAllPostsForHome } from "../lib/api";
import Hero from "../components/Hero";
import Content from "../components/Content";
import Banner from "../components/Banner";
import TeamSection from "../components/TeamSection";
import Navbar from "../components/navbar";

export default function Index({ allPosts: { edges }, preview }) {
  const heroPost = edges[0]?.node;
  const morePosts = edges.slice(1);

  return (
    <>
      <Layout preview={preview} NavVisible={true}>
        <Head>
          <title>Growii || Home</title>
        </Head>
        <div className="bg-hero-pattern bg-no-repeat">
          <Navbar NavVisible={true} className="z-10" />
          <Hero />
        </div>
        <Content />
        <Banner />
        <TeamSection />
        <h3 className="mt-36 text-center text-2xl md:text-6xl ">
          <span className="text-purple-700">Damos que hablar</span> en los
          medios
        </h3>
        <Container>
          {/* {heroPost && (
            <HeroPost
              title={heroPost.title}
              coverImage={heroPost.featuredImage?.node}
              date={heroPost.date}
              author={heroPost.author?.node}
              slug={heroPost.slug}
              excerpt={heroPost.excerpt}
            />
          )} */}
          {morePosts.length > 0 && <MoreStories posts={morePosts} />}
        </Container>
      </Layout>
    </>
  );
}

export async function getStaticProps({ preview = false }) {
  const allPosts = await getAllPostsForHome(preview);
  return {
    props: { allPosts, preview },
  };
}
