import { useState } from "react";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedinIn,
} from "react-icons/fa";
import Link from "next/link";
import Image from "next/image";
import NavLogo from "../public/growii-logo-text.svg";

const Navbar = ({
  showDirectorBoard,
  showAdminBoard,
  currentUser,
  NavVisible,
}) => {
  //Visibility Mobile Navbar
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <nav
        className={`flex w-full z-20 flex-row px-8 md:px-24 justify-between items-center text-xl font-Gilroy-Bold text-white ${
          NavVisible
            ? "bg-transparent"
            : " bg-gradient-to-r from-purple-700 to-black"
        } ${isOpen ? "fixed" : ""}  transform duration-700 ease-in-out`}
      >
        <div className="flex flex-row items-center justify-center">
          <Link href={"/"} className="cursor-pointer">
            <Image src={NavLogo} height={100} width={150} alt="Nav Logo" />
          </Link>
          {/* Desktop Nav */}
          <div className="hidden md:flex md:flex-row mt-2">
            <li className="ml-12 hover:text-purple-600 duration-300 cursor-pointer text-lg list-none font-medium font-poppins">
              Producto
            </li>

            <li className="pl-6 hover:text-purple-600 duration-300 cursor-pointer text-lg list-none font-medium font-poppins">
              Funciones
            </li>

            <li className="pl-6 hover:text-purple-600 duration-300 cursor-pointer text-lg list-none font-medium font-poppins">
              Precios
            </li>

            <li className="pl-6 hover:text-purple-600 duration-300 cursor-pointer text-lg list-none font-medium font-poppins">
              Nosotros
            </li>

            <li className="pl-6 hover:text-purple-600 duration-300 cursor-pointer text-lg list-none font-medium font-poppins">
              Prensa
            </li>
          </div>
        </div>
        <div className="hidden md:flex md:flex-row justify-center items-center">
          <li className="hover:text-purple-600 duration-300 cursor-pointer pr-6 text-lg list-none font-medium font-poppins">
            Log in
          </li>
          <button className="cursor-pointer bg-white text-purple-700 py-2 px-4 rounded-xl text-lg list-none font-medium font-poppins">
            Pruebalo gratis
          </button>
        </div>
        <button
          onClick={() => setIsOpen(!isOpen)}
          className="hover:cursor-pointer block md:hidden p-4 bg-growii-content text-growii-burger rounded-xl"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none font-medium"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d={!isOpen ? "M4 6h16M4 12h16M4 18h16" : "M6 18L18 6M6 6l12 12"}
            />
          </svg>
        </button>
      </nav>
      <div
        className={`transition transform ease-in-out duration-300 z-10 pt-16 fixed flex flex-col items-center justify-center min-w-full h-full w-full bg-growii-content opacity-90 ${
          !isOpen && "scale-0"
        }`}
      >
        <div className="w-5/6 text-center">
          <h2 className="py-6 cursor-pointer text-2xl tracking-widest  border-t border-white text-white">
            PRODUCTO
          </h2>
          <h2 className="py-6 cursor-pointer text-2xl tracking-widest  border-t border-white text-white">
            FUNCIONES
          </h2>
          <h2 className="py-6 cursor-pointer text-2xl tracking-widest  border-y border-white text-white">
            PRECIOS
          </h2>
          <h2 className="mt-12 py-4 cursor-pointer text-2xl tracking-widest  border-2 border-white text-white rounded-lg">
            LOGIN
          </h2>
          <div className="mt-24 flex flex-row justify-center text-4-xl text-white">
            <a className="px-8 text-4xl cursor-pointer">
              <FaLinkedinIn />
            </a>
            <a className="px-8 text-4xl  cursor-pointer">
              <FaTwitter />
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
