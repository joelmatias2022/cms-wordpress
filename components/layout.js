import Alert from "../components/alert";
import Footer from "./Footer";
import Navbar from "../components/navbar";
import Meta from "../components/meta";
import MessageBlubble from "./MessageBlubble";

export default function Layout({ NavVisible, preview, children }) {
  return (
    <>
      <Meta />
      {/* <div className="px-24"> */}
      {NavVisible ? "" : <Navbar NavVisible={NavVisible} />}
      <MessageBlubble />
      <div className="min-h-screen flex flex-col justify-center z-0">
        {/* <Alert preview={preview} /> */}
        <main>{children}</main>
      </div>
      <Footer />
      {/* </div> */}
    </>
  );
}
