const ContentCard = ({ firstSubtitle, secondParagraph }) => {
  return (
    <div className="mt-8 flex flex-col py-8 px-10 shadow-xl rounded-xl border-2 border-gray-200 w-full">
      <h3 className="text-2xl font-semibold text-growii-content">
        {firstSubtitle}
      </h3>
      <p className="mt-4 text-xl md:max-w-xl text-growii-gray">
        {secondParagraph}
      </p>
    </div>
  );
};

export default ContentCard;
