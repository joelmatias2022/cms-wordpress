import Image from "next/image";
import FirstImage from "../../public/img/ilustracion_01.png";
import SecondImage from "../../public/img/ilustracion_02.png";

import ContentStructure from "./ContentStructure";

const Content = () => {
  return (
    <div className="h-fullbg-gray-300">
      <div className="mt-32 text-center">
        <h2 className="text-6xl font-semibold text-growii-content">
          Dos fuentes de datos{" "}
        </h2>
        <span className="text-6xl font-semibold text-growii-content">
          para tomar{" "}
          <span className="text-growii-pink">mejores decisiones</span>
        </span>
      </div>
      <div className="mx-4 md:mx-36 mt-24 pb-36">
        <ContentStructure
          number={"01."}
          imageUsed={FirstImage}
          generalTitle={"Pregunta a tu equipo Directamente en Slack."}
          firstSubtitle={"Maximiza la efectividad"}
          firstParagraph={
            "Maximiza la receptividad y participacion de las preguntas al integrarte en su herramienta de trabajo"
          }
          secondSubtitle={"Total flexibilidad"}
          secondParagraph={
            "Todas las funcionalidades para diseniar el flujo de preguntas que mas te interese y quien mas te interese"
          }
          invert={false}
        />

        <ContentStructure
          number={"02."}
          imageUsed={SecondImage}
          generalTitle={"Monitorea el comportamiento organizativo en Slack"}
          firstSubtitle={"Conoce datos objetivos a tiempo real"}
          firstParagraph={
            "A traves del comportamiento se revela si la organizacion esta comprometida, sus debilidades y fortalezas reales mientras evitas, sobrecargar con demasiadas encuestas."
          }
          secondSubtitle={"Evolucion y tendencias"}
          secondParagraph={
            "Mucho mas certero que un dato aislad resulta la evolucion de este asi como la observacion de sus tendencias"
          }
          invert={true}
        />
      </div>
    </div>
  );
};

export default Content;
