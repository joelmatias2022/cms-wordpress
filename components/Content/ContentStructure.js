import Image from "next/image";
import ContentCard from "./ContentCard";

const ContentStructure = ({
  number,
  generalTitle,
  firstSubtitle,
  secondSubtitle,
  firstParagraph,
  secondParagraph,
  invert,
  imageUsed,
}) => {
  return (
    <div
      className={`flex ${
        invert ? "flex-col md:flex-row-reverse" : "flex-col md:flex-row"
      } items-center justify-center w-full`}
    >
      <div className="h-full w-full md:w-3/6">
        <Image src={imageUsed} layout="responsive" alt="Content Image" />
      </div>
      <div
        className={`${
          invert ? "mr-0 md:mr-16" : "ml-0 md:ml-16"
        } flex flex-col justify-center items-center md:items-start md:justify-start w-full min-w-full md:min-w-fit md:w-3/6`}
      >
        <h5 className="text-xl font-semibold text-growii-pink">{number}</h5>
        <h3 className="mt-4 text-center md:text-left text-2xl md:text-4xl font-semibold md:max-w-xl text-growii-content">
          {generalTitle}
        </h3>
        <div className="mt-8 flex flex-col w-full">
          <ContentCard
            firstSubtitle={firstSubtitle}
            secondParagraph={firstParagraph}
          />
          <ContentCard
            firstSubtitle={secondSubtitle}
            secondParagraph={secondParagraph}
          />
        </div>
      </div>
    </div>
  );
};

export default ContentStructure;
