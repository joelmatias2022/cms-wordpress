import Image from "next/image";
import FooterLogo from "../public/growii-logo-text.svg";

import {
  HiOutlineLocationMarker,
  HiOutlinePhone,
  HiOutlineMail,
} from "react-icons/hi";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedinIn,
} from "react-icons/fa";

export default function Footer() {
  return (
    <footer className="bg-growii-footer">
      <div className="px-24 lg:px-36 pb-24">
        <div className="pt-16 flex flex-col md:flex-row md:justify-between">
          <div className="flex flex-col md:flex-row md:items-center">
            <Image
              src={FooterLogo}
              height="100"
              width="150"
              alt="Growii Logo"
            />
            <h4 className="text-sm md:text-base text-white md:hidden text-center md:text-left">
              © Growii 2022, Todos los derechos reservados.
            </h4>
            <div className="ml-8 hidden md:flex flex-col md:flex-row text-gray-500">
              <h4 className="ml-8">Producto</h4>
              <h4 className="ml-8">Funciones</h4>
              <h4 className="ml-8">Precios</h4>
              <h4 className="ml-8">Nosotros</h4>
              <h4 className="ml-8">Prensa</h4>
            </div>
          </div>
          <div className="hidden md:flex flex-row items-center justify-center text-white">
            <span className="ml-8 hover:bg-purple-500 rounded-full duration-300 p-2 cursor-pointer">
              <FaFacebookF />
            </span>
            <span className="ml-8 hover:bg-purple-500 rounded-full duration-300 p-2 cursor-pointer">
              <FaInstagram />
            </span>
            <span className="ml-8 hover:bg-purple-500 rounded-full duration-300 p-2 cursor-pointer">
              <FaTwitter />
            </span>
            <span className="ml-8 hover:bg-purple-500 rounded-full duration-300 p-2 cursor-pointer">
              <FaLinkedinIn />
            </span>
          </div>
        </div>
        <div className="mt-10 flex flex-col md:flex-row justify-center text-center md:justify-between">
          <h4 className="text-white hidden md:block text-center md:text-left">
            Growii 2022, Todos los derechos reservados.
          </h4>
          <div className="flex flex-col justify-center md:flex-row text-white">
            <div className="flex flex-row items-center justify-center">
              <HiOutlineLocationMarker className="mr-2" /> Malaga, Spain
            </div>
            <div className="mt-8 md:mt-auto md:ml-8 flex flex-row items-center justify-center">
              <HiOutlinePhone className="mr-2" />
              +34 1245 5678
            </div>
            <div className="mt-8 md:mt-auto md:ml-8 flex flex-row items-center justify-center">
              <HiOutlineMail className="mr-2" />
              hola@growii.es
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
