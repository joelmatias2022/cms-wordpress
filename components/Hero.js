const Hero = () => {
  return (
    <div className="flex flex-col items-center content-center justify-center w-full bg-hero-pattern bg-center md:bg-auto bg-no-repeat md:min-h-screen">
      <div className="flex flex-col md:flex-row pb-36 mx-auto">
        <div className="w-full md:w-4/6 pt-36 flex flex-col justify-center items-center md:block">
          <h1 className="flex flex-col text-white text-3xl md:text-6xl font-poppins text-center md:text-left font-semibold">
            <span className=" md:px-auto">Conoces a tu equipo,</span>
            <span className="mt-4 flex flex-col md:flex-row text-purple-700">
              <span className="text-white"> pero...</span>¿los comprendes?
            </span>
          </h1>
          <p className="text-lg text-white mt-4 text-center md:text-left">
            Captura la opinion de tu equipo mientras monitoreas su
            comportamiento en Slack
          </p>
          <button className="mt-4 py-4 px-8 bg-purple-700 text-white rounded-lg">
            Pruebalo gratis
          </button>
          <div className="pt-4">
            <h6 className="text-purple-400">Confian en nosotros</h6>
            <div className="pt-2 flex flex-row justify-between w-20">
              <div className="flex flex-grow bg-purple-700 w-full">
                Microsoft
              </div>
              <div className="ml-2 flex flex-1 bg-purple-700 w-full">
                Google
              </div>
              <div className="ml-2 flex flex-1 bg-purple-700 w-full">Intel</div>
            </div>
          </div>
        </div>
        <div className="hidden md:block w-2/6 px-32 bg-purple-500 rounded-xl">
          <h2>Big cellphone image</h2>
        </div>
      </div>
    </div>
  );
};

export default Hero;
