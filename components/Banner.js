const Banner = () => {
  return (
    <div className="flex flex-col justify-center items-center p-4 md:p-18 bg-banner-container">
      <div className="mx-4 md:mx-36">
        <div className="flex flex-col md:flex-row justify-center md:justify-between rounded-xl bg-banner-pattern bg-center my-12 w-full p-8 md:mx-16 md:p-16">
          <div>
            <h3 className="text-white text-xl md:text-4xl max-w-xs md:max-w-xl">
              La alta tecnologia al servicio de la gestion del talento
            </h3>
          </div>
          <button className="mt-4 md:mt-0 text-xl font-bold px-6 py-2 rounded-full border-4 text-white border-purple-700">
            Pruebalo Gratis
          </button>
        </div>
      </div>
    </div>
  );
};

export default Banner;
