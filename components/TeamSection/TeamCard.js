import Image from "next/image";
import { FaTwitter, FaInstagram, FaLinkedinIn } from "react-icons/fa";

const TeamCard = ({ name, position, instagram, linkedin, twitter }) => {
  return (
    <div className="flex flex-col justify-center items-center rounded-xl border-2 border-gray-100 hover:bg-white duration-300 hover:shadow-2xl pt-8 pb-14 px-16 mr-8 cursor-pointer">
      <div className="p-8 bg-purple-600 rounded-full">
        <span>{"Pic"}</span>
        {/* <Image src={} height={} width={} alt="EMployee picture"/>  */}
      </div>
      <h4 className="mt-4 text-xl font-medium text-growii-content">{name}</h4>
      <h5 className="text-growii-gray">{position}</h5>
      <div className="mt-8 flex flex-row text-gray-500 duration-300">
        <span className="mr-0 md:mr-6 hover:bg-purple-500 hover:text-white rounded-full duration-300 p-2 cursor-pointer">
          <FaInstagram />
        </span>
        <span className="mr-0 md:mr-6 hover:bg-purple-500 hover:text-white rounded-full duration-300 p-2 cursor-pointer">
          <FaLinkedinIn />
        </span>
        <span className="mr-0 md:mr-6 hover:bg-purple-500 hover:text-white rounded-full duration-300 p-2 cursor-pointer">
          <FaTwitter />
        </span>
      </div>
    </div>
  );
};

export default TeamCard;
