import TeamCard from "./TeamCard";

const TeamSection = () => {
  return (
    <div className="mt-36 pb-24 bg-purple-200">
      <h2 className="font-medium pt-24 text-6xl text-center text-growii-pink">
        Nuestro Equipo
      </h2>
      <div className="flex flex-col justify-center items-center w-full">
        <p className="mt-4 text-2xl text-center text-growii-gray mx-2 md:px-0 md:max-w-4xl">
          Lorem ipsum dolor sit amer, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam
        </p>
      </div>
      <div className="mt-14 flex flex-col md:flex-row justify-center items-center">
        <TeamCard
          name="Clara Doe"
          position="cargo"
          instagram="instagram"
          linkedin="linkedin"
          twitter="twitter"
        />
        <TeamCard
          name="Elias Doe"
          position="cargo"
          instagram="instagram"
          linkedin="linkedin"
          twitter="twitter"
        />
        <TeamCard
          name="Sofia Doe"
          position="cargo"
          instagram="instagram"
          linkedin="linkedin"
          twitter="twitter"
        />
        <TeamCard
          name="Sergio Doe"
          position="cargo"
          instagram="instagram"
          linkedin="linkedin"
          twitter="twitter"
        />
      </div>
      <h2 className="pt-12 text-center">Add arrows and slider</h2>
      <div className="flex flex-row justify-center items-center">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M7 16l-4-4m0 0l4-4m-4 4h18"
          />
        </svg>
      </div>
    </div>
  );
};

export default TeamSection;
